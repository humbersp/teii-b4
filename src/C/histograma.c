// C program to perform histogram equalisation to adjust contrast levels 
  
// All the needed library functions for this program 
#include <fcntl.h> 
#include <math.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h>
#include "histograma.h"
  
// Function to perform histogram equalisation on an image 
// Function takes total rows, columns, input file name and output 
// file name as parameters 
void histogramEqualisation(int width, int height, unsigned char* img, long long* hist) 
{ 
    int n = width*height;
    for(int i = 0; i < n; i++)
	hist[img[i]]++;
} 
  
// driver code 
int main() 
{ 
	/*
    // declaring variables 
    char* input_file_name; 
    char* output_file_name; 
    int cols, rows; 
  
    // defining number of rows and columns in an image 
    // here, image size is 512*512 
    cols = 512; 
    rows = 512; 
  
    // defining input file name (input image name) 
    // this boat_512_512 is a raw grayscale image 
    input_file_name = "fondo.jpg"; 
  
    // calling function to do histogram equalisation 
    int *hist = histogramEqualisation(cols, rows, input_file_name);

    for(int i = 0 ; i < 256 ; i++)
    	printf("%d, ", &hist[i]);
  */
    return 0; 
}
